﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Controller_AI))]
public class AI_Senses :MonoBehaviour
{
    [SerializeField] [Range(0,180)] float FOV;
    [SerializeField] [Min(0)]public float sightDistance;
    [SerializeField] [Min(0)]public int hearingDistance;
    [SerializeField] [Min(0)]public int reinforcementRange;

    [Header("Gizmos")]
    [SerializeField] Color VisionColor;
    [SerializeField] Color HearingColor;
    [SerializeField] Color ReinforceRangeColor;
    Pawn_AI pawn;

   
    bool Initailized = false;

    void Initalize()
    {
        pawn = GetComponent<Controller_AI>().pawn as Pawn_AI;
        Initailized = true;
    }

    public bool CanSeePlayer()
    {
        if (!Initailized)
        {
            Initalize();
        }

        if (pawn.pollingStation != null)
        {
            if(pawn.pollingStation.player!= null)
            {
                return true;
            }
        }

        Pawn player = GameManager.instance.player.pawn;
        Vector3 vectorToTarget = player.transform.position-  pawn.transform.position;
        //if the player is within the feild of view
        if( Vector3.Angle(pawn.transform.up,vectorToTarget) <= FOV)
        {
            //and the player is within the vision range
            if(vectorToTarget.magnitude <= sightDistance)
            {
                RaycastHit2D hit;
                hit = Physics2D.Raycast(pawn.transform.position, vectorToTarget);
                if (hit.collider != null)
                {
                    if (hit.collider.gameObject.GetComponent<Pawn_Player>() != null)
                    {
                        return true;
                    }
                }
               
            }
        }
        return false;
    }

    public bool CanHearNoise(int volume, Vector2Int pos)
    {
        if (!Initailized)
        {
            Initalize();
        }

        Pawn player = GameManager.instance.player.pawn;

        return PathFinder.checkHearingRadius(pawn.gridPosition, pos, volume, hearingDistance);
    }

    private void OnDrawGizmos()
    {
        if (!Initailized)
        {
            Initalize();
        }

        Gizmos.color = VisionColor;
        
 
        Gizmos.DrawLine(pawn.transform.position, pawn.transform.position + pawn.transform.up * sightDistance);
        Vector3 RightLookingVector = (Quaternion.Euler(0, 0, -FOV) * pawn.transform.up).normalized;
        Gizmos.DrawLine(pawn.transform.position, pawn.transform.position + RightLookingVector * sightDistance);
        Vector3 LeftLookingVector = (Quaternion.Euler(0, 0, FOV) * pawn.transform.up).normalized;
        Gizmos.DrawLine(pawn.transform.position, pawn.transform.position + LeftLookingVector * sightDistance);


        Gizmos.color = HearingColor;
        Gizmos.DrawWireSphere(pawn.transform.position, hearingDistance);

        Gizmos.color = ReinforceRangeColor;
        Gizmos.DrawWireSphere(pawn.transform.position, reinforcementRange);
    }
}
