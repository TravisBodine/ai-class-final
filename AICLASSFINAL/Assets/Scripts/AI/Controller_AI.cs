﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AI_Senses))]
public class Controller_AI : Controller, ICanRecieveMessages
{

   public enum AIStates
    {
        Idle,
        Patrol,
        Alert,
        Chase,
        Stunned,
        Dead
    }

    public AIStates currentState;

    public List<Node> Path = new List<Node>();
    int currentNode = 0;
    [SerializeField] protected Patrol patrol;
    [SerializeField] protected AI_Senses senses;

    

    int stunnedTurnsRemaining = 0;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.enemies.Add(this);
        EventSystem.SubscribeToEvent("Noise", this);
        EventSystem.SubscribeToEvent("PlayerMoved", this);
        EventSystem.SubscribeToEvent("CallForHelp", this);

        pawn.gridPosition = GameBoard.instance.GetGridPosition(pawn.transform.position);
        pawn.transform.position = GameBoard.instance.GetPositionOfGridSquare(pawn.gridPosition);

        if (patrol.HasPatrolPoints())
        {
            currentState = AIStates.Patrol;
            Path = PathFinder.FindMoveMentPath(pawn.gridPosition, patrol.currentNode);
        }
        else
        {
            currentState = AIStates.Idle;
        }
    }

    public void TakeTurn()
    {
        switch (currentState)
        {
            case AIStates.Idle:
                Idle();
                break;
            case AIStates.Patrol:
                Patrol();
                break;
            case AIStates.Alert:
                Alert();
                break;
            case AIStates.Chase:
                Chase();
                break;
            case AIStates.Stunned:
                Stunned();
                break;
            case AIStates.Dead:
                
                break;
            default:
                break;
        }


    }


    private void Idle()
    {
        if (patrol.HasPatrolPoints())
        {
            currentState = AIStates.Patrol;
        }


    }

    void Patrol()
    {
        if (Path.Count == 0)
        {
            currentNode = 0;
            patrol.IncrimentPatrolNode();
            Path = PathFinder.FindMoveMentPath(pawn.gridPosition, patrol.currentNode);
        }
        pawn.Move(Path[currentNode].pos.x - pawn.gridPosition.x, Path[currentNode].pos.y - pawn.gridPosition.y);
        currentNode++;

        if (currentNode >= Path.Count)
        {
            currentNode = 0;

            Path = PathFinder.FindMoveMentPath(pawn.gridPosition, patrol.currentNode);
        }

        if (senses.CanSeePlayer())
        {
            currentState = AIStates.Chase;
        }
    }

    private void Alert()
    {
        if (currentNode >= Path.Count)
        {
            currentNode = 0;
            Path = PathFinder.FindMoveMentPath(pawn.gridPosition, patrol.currentNode);
            currentState = AIStates.Patrol;
        }
        else
        {
            pawn.Move(Path[currentNode].pos.x - pawn.gridPosition.x, Path[currentNode].pos.y - pawn.gridPosition.y);
            currentNode++;
        }


    }

    private void Chase()
    {
        currentNode = 0;
        //if we are one away from the player attack intead
        //if()
        Path = PathFinder.FindMoveMentPath(pawn.gridPosition, GameManager.instance.player.pawn.gridPosition);

        Pawn check = GameBoard.instance.GetPawnAtPosition(Path[currentNode].pos);
        if (check != null)
        {
            pawn.Attack(check);
        }
        else
        {
            pawn.Move(Path[currentNode].pos.x - pawn.gridPosition.x, Path[currentNode].pos.y - pawn.gridPosition.y);
            currentNode++;

        }


        if (!senses.CanSeePlayer())
        {
            currentState = AIStates.Alert;
        }
    }

    public void Stun(int turns, int hearingReduction, float seeingReduction)
    {
        senses.hearingDistance -= hearingReduction;
        senses.sightDistance -= seeingReduction;

        senses.hearingDistance = Mathf.Max(senses.hearingDistance, 0);
        senses.sightDistance = Mathf.Max(senses.sightDistance, 0);

        stunnedTurnsRemaining = turns;
        currentState = AIStates.Stunned;
    }
    private void Stunned()
    {
        stunnedTurnsRemaining--;
        if (stunnedTurnsRemaining <= 0)
        {
            currentState = AIStates.Alert;
        }
    }
    public void Die()
    {
        GameManager.instance.enemies.Remove(this);
        Destroy(pawn.gameObject);
        Destroy(gameObject);
    }

    public void ReceiveMessage<T>(string eventToTrigger, T genericType)
    {
        if (currentState == AIStates.Dead)
        {
            return;
        }

        if (eventToTrigger == "Noise")
        {
            
            if (currentState != AIStates.Chase)
            {
                
                HearingData data = genericType as HearingData;
                if (data != null)
                {
                    bool canHear = senses.CanHearNoise(data.Volume, data.Position);
//                    Debug.Log("can Hear = " + canHear + data.Volume + data.Position);
                    if (canHear)
                    {
                      
                        currentNode = 0;
                        Path = PathFinder.FindMoveMentPath(pawn.gridPosition, data.Position);

                        currentState = AIStates.Alert;
                        EventSystem.TriggerEvent("CallForHelp", pawn.gridPosition);
                    }
                }
            }
        }
        else if (eventToTrigger == "PlayerMoved")
        {
            if (pawn.isDead)
            {
                currentState = AIStates.Dead;
                return;
            }
            if (senses.CanSeePlayer())
            {
                currentState = AIStates.Chase;
            }
        }
        else if (eventToTrigger == "CallForHelp")
        {
          
            if (currentState == AIStates.Chase )
            {
                return;
            }

            Vector2Int? pos = genericType as Vector2Int?;
            if(pos != null)
            {
                Vector2Int otherAIPos = (Vector2Int)pos;
                //if (pawn == GameBoard.instance.GetPawnAtPosition(otherAIPos))
                //{
                //    return;
                //}
                int dist = Mathf.Abs(pawn.gridPosition.x - otherAIPos.x) + Mathf.Abs(pawn.gridPosition.y - otherAIPos.y);
                Debug.Log(dist);
                if (dist <= senses.reinforcementRange)
                {
                    currentState = AIStates.Alert;
                    currentNode = 0;
                    Path = PathFinder.FindMoveMentPath(pawn.gridPosition, otherAIPos);
                }
            }
        }
    }
}
