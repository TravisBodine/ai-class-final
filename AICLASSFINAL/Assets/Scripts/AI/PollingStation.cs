﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PollingStation : MonoBehaviour
{
    //public List<Controller_AI> enemies = null;
    public Pawn_Player player = null;
    // Start is called before the first frame update



    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.gameObject);
        Pawn_Player foundPlayer = collision.gameObject.GetComponent<Pawn_Player>();
        if(foundPlayer != null)
        {
            player = foundPlayer;
            return;
        }

        Pawn_AI foundEnemy = collision.gameObject.GetComponent<Pawn_AI>();
        if (foundEnemy != null)
        {
            foundEnemy.pollingStation = this;
            return;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
   
        Pawn_Player foundPlayer = collision.gameObject.GetComponent<Pawn_Player>();
        if (foundPlayer != null)
        {
            player = null;
            return;
        }

        Pawn_AI foundEnemy = collision.gameObject.GetComponent<Pawn_AI>();
        if (foundEnemy != null)
        {
            foundEnemy.pollingStation = this;
            return;
        }
    }
}
