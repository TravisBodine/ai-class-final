﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Patrol
{
    [SerializeField] List<Vector2Int> patrolNodes;

    //public enum PatrolType
    //{
    //    Loop,
    //    BackAndForth
    //}
    //public PatrolType patrolType;
    public Vector2Int currentNode
    {
        get
        {
            return patrolNodes[currentNodeIndex];
        }
        private set
        {
            currentNode = value;
        }
    }
    int currentNodeIndex = 0;
    bool incrimenting = true;

    public void IncrimentPatrolNode()
    {
        currentNodeIndex++;

        if (currentNodeIndex >= patrolNodes.Count)
        {
            currentNodeIndex = 0;

            //if(patrolType == PatrolType.Loop)
            //{
            //    currentNode = 0;
            //}
            //else if(patrolType == PatrolType.BackAndForth)
            //{
            //    incrimenting = false;
            //}
        }
    }

    public bool HasPatrolPoints()
    {
        return patrolNodes.Count > 0;
    }
}
