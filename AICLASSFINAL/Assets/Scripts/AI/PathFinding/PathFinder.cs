﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PathFinder
{
    [System.Serializable]
    public class NodeConnection
    {
        public Node fromNode;
        public Node toNode;
        public float cost;
    }

    [System.Serializable]
    public class NodeRecord
    {
        public Node node;
        public NodeConnection connection;
        public float totalCostFromStart;
    }

    public static List<Node> FindMoveMentPath(Vector2Int startingPos, Vector2Int Goal)
    {
        List<NodeRecord> openList = new List<NodeRecord>();
        List<NodeRecord> closedList = new List<NodeRecord>();
       
        NodeRecord start = new NodeRecord();
        start.node = GameBoard.instance.GetNode(startingPos);

        start.totalCostFromStart = 0;

        NodeRecord currNode = null;

        openList.Add(start);

       
        //whilenot found goal and open list has nodes
        while (openList.Count > 0)
        {

            //find node with lowest weight in open list 
            currNode = SmallestElement(openList);
            //Debug.Log(currNode.node);
            // check for goal
            if (currNode.node == GameBoard.instance.GetNode(Goal))
            {             
                break;
            }

            //addchildren and weights
            foreach (var connection in currNode.node.connections)
            {
                //check blocked
                if (connection.canMoveThrough)
                {
                    if (ListContains(closedList, connection))
                    {
                        continue;
                    }
                    else if (ListContains(openList, connection))
                    {
                        //check if weight is lower and if it is then replace it

                        //i just doing 1 or cant walk for the walk weights the hearing is going to have diffrent weight
                        if (FindInList(openList, currNode.node).totalCostFromStart + 1 + findEstimateToGoal(currNode.node, GameBoard.instance.GetNode(Goal)) < FindInList(openList, connection).totalCostFromStart)
                        {
                            openList.Remove(FindInList(openList, connection));

                            NodeRecord newNodeRecord = new NodeRecord();
                            NodeConnection newNodeConnection = new NodeConnection();
                            newNodeConnection.fromNode = currNode.node;
                            newNodeConnection.toNode = connection;
                            newNodeRecord.connection = newNodeConnection;
                            newNodeRecord.node = connection;
                            //total cost for this node is the previous nodes cost plus the connection to this node

                            newNodeRecord.totalCostFromStart = FindInList(openList, currNode.node).totalCostFromStart + 1 + findEstimateToGoal(connection, GameBoard.instance.GetNode(Goal));
                            
                            openList.Add(newNodeRecord);
                        }
                       
                    }
                    else
                    {
                        // make node record
                        NodeRecord newNodeRecord = new NodeRecord();
                        NodeConnection newNodeConnection = new NodeConnection();
                        newNodeConnection.fromNode = currNode.node;
                        newNodeConnection.toNode = connection;
                        newNodeRecord.connection = newNodeConnection;
                        newNodeRecord.node = connection;

                        newNodeRecord.totalCostFromStart = FindInList(openList, currNode.node).totalCostFromStart + 1 + findEstimateToGoal(currNode.node, GameBoard.instance.GetNode(Goal));

                        openList.Add(newNodeRecord);

                    }
                }
            }

            //remove from open and add to closed
            closedList.Add(currNode);
            openList.Remove(currNode);
        }
        ///done with fincing path or ran out of nodes
        List<Node> path = new List<Node>();

        //if its not the goal then we ran out of nodes and dont have a path
        if (currNode.node != GameBoard.instance.GetNode(Goal))
        {
            Debug.Log("not goal");
            // We ran out of nodes without finding a goal
            // Clear the path
            path.Clear();

            // Quit the function
            return null;
        }

        //otherwise recreate the path from the node connections
        while (currNode.node != GameBoard.instance.GetNode(startingPos))
        {
          
            //Debug.Log(currNode.connection);
            path.Add(currNode.node);

            currNode = FindInList(closedList, currNode.connection.fromNode);
        }

        //reverse the path so it now goes from start to finish
        path.Reverse();

        return path;
    }

    public static bool checkHearingRadius(Vector2Int startingPos, Vector2Int Goal,int volume,int hearingAbility)
    {

        int totalHearingDistance = volume + hearingAbility;

        List<NodeRecord> openList = new List<NodeRecord>();
        List<NodeRecord> closedList = new List<NodeRecord>();

        NodeRecord start = new NodeRecord();
        start.node = GameBoard.instance.GetNode(startingPos);

        start.totalCostFromStart = 0;

        NodeRecord currNode = null;

        openList.Add(start);

      

        //whilenot found goal and open list has nodes
        while (openList.Count > 0)
        {

            //find node with lowest weight in open list 
            currNode = SmallestElement(openList);
            //Debug.Log(currNode.node);
            // check for goal
            if (currNode.node == GameBoard.instance.GetNode(Goal))
            {
                break;
            }

            //addchildren and weights
            foreach (var connection in currNode.node.connections)
            {
                
                //check blocked
                if (connection.canHearThrough)
                {
                 
                    if (ListContains(closedList, connection))
                    {
                        continue;
                    }
                    else if (ListContains(openList, connection))
                    {
                        //check if weight is lower and if it is then replace it
                        if (FindInList(openList, currNode.node).totalCostFromStart + connection.hearWeight < FindInList(openList, connection).totalCostFromStart)
                        {
                            openList.Remove(FindInList(openList, connection));

                            NodeRecord newNodeRecord = new NodeRecord();
                            NodeConnection newNodeConnection = new NodeConnection();
                            newNodeConnection.fromNode = currNode.node;
                            newNodeConnection.toNode = connection;
                            newNodeRecord.connection = newNodeConnection;
                            newNodeRecord.node = connection;
                            //total cost for this node is the previous nodes cost plus the connection to this node

                            newNodeRecord.totalCostFromStart = FindInList(openList, currNode.node).totalCostFromStart + connection.hearWeight ;

                            openList.Add(newNodeRecord);
                        }

                    }
                    else
                    {
                        if(FindInList(openList, currNode.node).totalCostFromStart + connection.hearWeight <= totalHearingDistance)
                        {
                            // make node record
                            NodeRecord newNodeRecord = new NodeRecord();
                            NodeConnection newNodeConnection = new NodeConnection();
                            newNodeConnection.fromNode = currNode.node;
                            newNodeConnection.toNode = connection;
                            newNodeRecord.connection = newNodeConnection;
                            newNodeRecord.node = connection;

                            newNodeRecord.totalCostFromStart = FindInList(openList, currNode.node).totalCostFromStart + connection.hearWeight;

                            openList.Add(newNodeRecord);
                        }
                    }
                }
            }

            //remove from open and add to closed
            closedList.Add(currNode);
            openList.Remove(currNode);
        }

        //if its not the goal then we ran out of nodes and dont have a path
        if (currNode.node != GameBoard.instance.GetNode(Goal))
        {
            return false;
        }
        else
        {
            return true;
        }

        //return false;
    }

    private static float findEstimateToGoal(Node node, Node targetNode)
    {
        return Mathf.Abs(node.pos.x - targetNode.pos.x) + Mathf.Abs(node.pos.y - targetNode.pos.y);
    }

    private static NodeRecord FindInList(List<NodeRecord> targetList, Node testNode)
    {

        foreach (var nodeRecord in targetList)
        {
            if (nodeRecord.node == testNode)
            {
                return nodeRecord;
            }
        }
        return null;
    }

    private static NodeRecord SmallestElement(List<NodeRecord> targetList)
    {
        NodeRecord smallestNode = null;

        foreach (var nodeRecord in targetList)
        {
            if (smallestNode == null || nodeRecord.totalCostFromStart < smallestNode.totalCostFromStart)
            {
                smallestNode = nodeRecord;
            }
        }
        return smallestNode;
    }

    private static bool ListContains(List<NodeRecord> testList, Node testNode)
    {
        foreach (var nodeRecord in testList)
        {
            if (nodeRecord.node == testNode)
            {
                return true;
            }
        }
        return false;
    }
}
