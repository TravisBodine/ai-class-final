﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn_AI : Pawn
{
    public PollingStation pollingStation;

    public override void Die()
    {
        base.Die();
        gameObject.SetActive(false);
    }
}
