﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Health 
{
    int _health;
    public int health
    {
        get { return _health; }

        private set
        {
            _health = value;
            //EventSystem.TriggerEvent("HealthChanged", _health);
        }
    }
    [SerializeField] public int maxHealth;

    public float healthPercentage {
        get
        {
            return health / maxHealth;
        }
    }

   public void TakeDamage(int amount)
    {
        if(amount >= 0)
        {
            health -= amount;
            health = Mathf.Clamp(health, 0, maxHealth);
        }
    }

    public void Heal(int amount)
    {
        if (amount >= 0)
        {
            health += amount;
            health = Mathf.Clamp(health, 0, maxHealth);
        }
    }

    public void FullHeal()
    {
        health = maxHealth;
    }
}
