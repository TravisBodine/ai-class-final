﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour,ICanRecieveMessages
{
    [SerializeField] Vector3 offset = new Vector3(0, 0, -10);
    void Start()
    {
        EventSystem.SubscribeToEvent("PlayerMoved", this);

    }

    public void ReceiveMessage<T>(string eventType,T genericType)
    {
        transform.position = GameManager.instance.player.pawn.gameObject.transform.position + offset;
    }

}
