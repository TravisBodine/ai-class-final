﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public Controller_Player player;

    public List<Controller_AI> enemies = new List<Controller_AI>();
    public List<Controller_AI> deadEnemies = new List<Controller_AI>();

    [SerializeField] float turnDelay;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);

        }
        else
        {
            Destroy(gameObject);
        }
    }

  
    public void TakeEnemiesTurn()
    {
        StartCoroutine(WaitForTurnDelay());
     
    }
    

    IEnumerator WaitForTurnDelay()
    {
        yield return new WaitForSeconds(turnDelay);

        foreach (Controller_AI enemy in enemies)
        {
            enemy.TakeTurn();
        }

        foreach (Controller_AI controller in deadEnemies)
        {
            enemies.Remove(controller);
            controller.Die();
        }
        deadEnemies.Clear();

        yield return new WaitForSeconds(turnDelay);
        player.StartTurn();
    }
}
