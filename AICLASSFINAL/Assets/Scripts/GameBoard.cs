﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBoard : MonoBehaviour
{
    public static GameBoard instance;

    Node[] nodes;

    public Vector2Int mapsize;

    public Node[,] nodePositions { get; private set; }

    public float gridSpacing => grid.cellSize.x;
    Grid grid;

    int? lowestX = null;
    int? lowestY = null;
    int? highestX = null;
    int? highestY = null;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

            grid = GetComponentInChildren<Grid>();
            InitalizeNodes();

            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    void InitalizeNodes()
    {
        nodes = FindObjectsOfType<Node>();
        if (nodes.Length < 0)
        {
            return;
        }

        foreach (Node node in nodes)
        {

            Vector3Int pos = grid.WorldToCell(node.gameObject.transform.position);
            if (lowestX == null || pos.x < lowestX)
            {
                lowestX = pos.x;
            }
            if (highestX == null || pos.x > highestX)
            {
                highestX = pos.x;
            }


            if (lowestY == null || pos.y < lowestY)
            {
                lowestY = pos.y;
            }
            if (highestY == null || pos.y > highestY)
            {
                highestY = pos.y;
            }
        }

        mapsize = new Vector2Int((int)(highestX - lowestX), (int)(highestY - lowestY));

        nodePositions = new Node[mapsize.x + 1, mapsize.y + 1];

        foreach (Node node in nodes)
        {
            Vector3Int pos = grid.WorldToCell(node.gameObject.transform.position);
            //Debug.Log(pos+ " "+ lowestX + " " + lowestY);
            nodePositions[(int)(pos.x - lowestX), (int)(pos.y - lowestY)] = node;
            Vector2Int nodepos = new Vector2Int((int)(pos.x - lowestX), (int)(pos.y - lowestY));
            node.pos = nodepos;

        }

        ConnectNodes();
    }

    void ConnectNodes()
    {
        for (int x = 0; x < nodePositions.GetLength(0); x++)
        {
            for (int y = 0; y < nodePositions.GetLength(1); y++)
            {
                if(x >0)
                {
                    nodePositions[x, y].connections.Add(nodePositions[x - 1, y]);
                }
                if (y > 0)
                {
                    nodePositions[x, y].connections.Add(nodePositions[x , y-1]);
                }
                if (x < nodePositions.GetLength(0)-1)
                {
                    nodePositions[x, y].connections.Add(nodePositions[x + 1, y]);
                }
                if (y < nodePositions.GetLength(0)-1 )
                {
                    nodePositions[x, y].connections.Add(nodePositions[x, y + 1]);
                }
            }
        }
    }

    public Vector2Int GetGridPosition(Vector3 pos)
    {
        Vector3Int convetedPos = grid.WorldToCell(pos);
        Vector2Int gridPos = new Vector2Int((int)(convetedPos.x - lowestX), (int)(convetedPos.y - lowestY));

        return gridPos;
    }

    public Vector3 GetPositionOfGridSquare(Vector2Int gridPos)
    {

        Vector3Int convertedToGrid = new Vector3Int((int)(gridPos.x + lowestX), (int)(gridPos.y + lowestY), 0);

        Vector3 convetedPos = grid.GetCellCenterWorld(convertedToGrid);
        convetedPos.z = 0;
       // Debug.Log(lowestX + " " + lowestY);
        //Debug.Log(gridPos + " " + convertedToGrid + " " + convetedPos);
        return convetedPos;
    }

    public Node GetNode(Vector2Int pos)
    {
        if (!isValidGridPosition(pos.x, pos.y))
        {
            return null;
        }
        return nodePositions[pos.x, pos.y];
    }

    public bool isValidGridPosition(int x, int y)
    {
        if (x > nodePositions.GetLength(0)-1 || y > nodePositions.GetLength(1)-1 || x < 0 || y < 0)
        {
            return false;
        }else if (!nodePositions[x, y].canMoveThrough)
        {
            return false;
        }
        return true;
    }

    public Pawn GetPawnAtPosition(Vector2Int pos)
    {
        //idk about this, could do a dictionary<Vector2Int,Pawn> of every position but that would get super big
        foreach (Controller_AI controller in GameManager.instance.enemies)
        {
            if(controller.pawn.gridPosition == pos)
            {
                return controller.pawn;
            }
        }
        if(GameManager.instance.player.pawn.gridPosition == pos)
        {
            return GameManager.instance.player.pawn;

        }

        return null;
    }

    public Controller GetControllerAtPosition(Vector2Int pos)
    {
        //idk about this, could do a dictionary<Vector2Int,Pawn> of every position but that would get super big
        foreach (Controller_AI controller in GameManager.instance.enemies)
        {
            if (controller.pawn.gridPosition == pos)
            {
                return controller;
            }
        }
        if (GameManager.instance.player.pawn.gridPosition == pos)
        {
            return GameManager.instance.player;

        }

        return null;
    }
}
