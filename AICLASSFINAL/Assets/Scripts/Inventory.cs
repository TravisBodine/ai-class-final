﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Inventory :MonoBehaviour
{

    //this should be moves into its own weapon script or something
    [SerializeField]public int FlashBangAmount= 3;
    [SerializeField]int FlashBangStunTurns = 1;

    [SerializeField] int hearingReduction = 1;
    [SerializeField] float seeingReduction = 10;
                       
    public void UseFlashBang(Vector2Int position)
    {
        if(FlashBangAmount <= 0)
        {
            return;
        }

        Controller_AI controller =  GameBoard.instance.GetControllerAtPosition(position) as Controller_AI;
        if(controller != null)
        {
            controller.Stun(FlashBangStunTurns,hearingReduction,seeingReduction);
        }

        FlashBangAmount--;
        EventSystem.TriggerEvent("UsedItem", FlashBangAmount);
        
    }
}
