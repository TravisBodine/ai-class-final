﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour
{
    public Health health;
    public int damage = 1;
    public Vector2Int gridPosition = new Vector2Int();

    public bool isDead = false;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        gridPosition = GameBoard.instance.GetGridPosition(transform.position);
        transform.position = GameBoard.instance.GetPositionOfGridSquare(gridPosition);
        health.FullHeal();
    }

    public virtual bool Move(int xAmount, int yAmount)
    {
        if(GameBoard.instance.isValidGridPosition(gridPosition.x + xAmount, gridPosition.y + yAmount))
        {
            gridPosition.x += xAmount;
            gridPosition.y += yAmount;

            transform.position = GameBoard.instance.GetPositionOfGridSquare(gridPosition);
            RotateCharacterToLookForward(xAmount, yAmount);
            return true;   
        }
        else
        {
            return false;
        }
    }

   protected void RotateCharacterToLookForward(int xDir,int yDir)
    {
        Vector3 rotatation = new Vector3(0, 0, 0);

        if (xDir > 0)
        {
            rotatation.z = -90;
        }
        else if(xDir <0)
        {
            rotatation.z = 90;
        }
        if (yDir > 0)
        {
            rotatation.z = 0;
        }
        else if (yDir < 0)
        {
            rotatation.z = 180;
        }

        transform.rotation = Quaternion.Euler(rotatation);
    }

    public virtual void TakeDamage(int amount)
    {
        health.TakeDamage(amount);

        if(health.health <= 0)
        {
            Die();
        }
    }

    public virtual void Heal(int amount)
    {
        health.Heal(amount);
    }

    public virtual void Attack(Pawn pawn)
    {
        pawn.TakeDamage(damage);
    }

    public virtual void Die()
    {
        isDead = true;
    }
    
}
