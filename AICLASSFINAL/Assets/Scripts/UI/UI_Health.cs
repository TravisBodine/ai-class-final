﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Health : MonoBehaviour,ICanRecieveMessages
{
    [SerializeField] Image ImagePrefab;
    [SerializeField] Sprite HealthIcon;
    [SerializeField] Transform grid;

    List<Image> images = new List<Image>();

    bool Initialized = false;

    private void Start()
    {
        EventSystem.SubscribeToEvent("PlayerTookDamage", this);
        Initialize();
    }

    void Initialize()
    {
        for (int i = 0; i < GameManager.instance.player.pawn.health.maxHealth; i++)
        {
            Image newImage =Instantiate(ImagePrefab, grid);
            images.Add(newImage);
            newImage.sprite = HealthIcon;
        }
        Initialized = true;
    }

    public void UpdateUI()
    {
        if (!Initialized)
        {
            Initialize();
        }

        int i = 0;
        for(;i< GameManager.instance.player.pawn.health.health;i++)
        {
            images[i].enabled = true;
        }
        for (; i < GameManager.instance.player.pawn.health.maxHealth; i++)
        {
            images[i].enabled = false;
        }
    }

    public void ReceiveMessage<T>(string eventType,T genericType)
    {
        UpdateUI();
    }
}
