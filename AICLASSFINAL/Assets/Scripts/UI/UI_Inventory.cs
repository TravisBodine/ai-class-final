﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Inventory : MonoBehaviour,ICanRecieveMessages
{ 
    public Text text;
    [SerializeField] Inventory inventory;
 

    // Start is called before the first frame update
    void Start()
    {
       // text = GetComponent<Text>();
        EventSystem.SubscribeToEvent("UsedItem", this);
    }

    public void UseFlashBang()
    {
        foreach (var enemy in GameManager.instance.enemies)
        {
            inventory.UseFlashBang(enemy.pawn.gridPosition);
        }
        
    }

    public void ReceiveMessage<T>(string eventType,T genericType)
    {
         text.text = "x " + inventory.FlashBangAmount;
    }
}
