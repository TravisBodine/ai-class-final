﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HearingData
{
    public Vector2Int Position;
    public int Volume;
}

public class Pawn_Player : Pawn
{
    public int footStepVolume;



    public override bool Move(int xAmount, int yAmount)
    {
        //this should probally be in the controller
        EventSystem.TriggerEvent("PlayerMoved",gridPosition);
        Pawn check = GameBoard.instance.GetPawnAtPosition(new Vector2Int(gridPosition.x +xAmount,gridPosition.y+ yAmount));
        if (check != null)
        {
            RotateCharacterToLookForward(xAmount, yAmount);
           Attack(check);
            return true;
        }
        else
        {   
            if (GameBoard.instance.isValidGridPosition(gridPosition.x + xAmount, gridPosition.y + yAmount))
            {
                gridPosition.x += xAmount;
                gridPosition.y += yAmount;

                transform.position = GameBoard.instance.GetPositionOfGridSquare(gridPosition);
                RotateCharacterToLookForward(xAmount, yAmount);

                HearingData data = new HearingData();
                data.Position = new Vector2Int(gridPosition.x + xAmount, gridPosition.y + yAmount);
                data.Volume = footStepVolume;
                EventSystem.TriggerEvent("Noise", data);

                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public override void TakeDamage(int amount)
    {
        base.TakeDamage(amount);
        EventSystem.TriggerEvent("PlayerTookDamage", health.health);
    }

}
