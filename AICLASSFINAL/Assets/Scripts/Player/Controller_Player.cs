﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player : Controller
{
    [SerializeField] float inputWaitAmount;

    enum PlayerControllerStates
    {
        Wait,
        Input
    }
    PlayerControllerStates currentState;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.player = this;
        currentState = PlayerControllerStates.Input;
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case PlayerControllerStates.Wait:
                break;
            case PlayerControllerStates.Input:
                Input();
                break;
        }

     
    }

    private void Input()
    {
        bool actionTaken = false;

       

        if (UnityEngine.Input.GetKeyDown(KeyCode.W))
        {
            actionTaken = pawn.Move(0, 1);
        
        }
        else if (UnityEngine.Input.GetKeyDown(KeyCode.S))
        {
            actionTaken = pawn.Move(0, -1);
         
        }
        else if (UnityEngine.Input.GetKeyDown(KeyCode.A))
        {
            actionTaken = pawn.Move(-1, 0);
          
        }
        else if (UnityEngine.Input.GetKeyDown(KeyCode.D))
        {
            actionTaken = pawn.Move(1, 0);
            
        }


        if (actionTaken)
        {
            currentState = PlayerControllerStates.Wait;
            GameManager.instance.TakeEnemiesTurn();
        }
    }

    public void StartTurn()
    {
        currentState = PlayerControllerStates.Input;
    }
}
