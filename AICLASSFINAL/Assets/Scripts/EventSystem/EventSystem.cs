﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EventSystem 
{
    static Dictionary<string, List<ICanRecieveMessages>> eventsAndSubscribers = new Dictionary<string, List<ICanRecieveMessages>>();


    /// <summary>
    /// Adds a listener to the Event and creates an event if there isnt one already
    /// </summary>
    /// <param name="eventToSubscribeTo"></param>
    /// <param name="subscriber"></param>
    public static void SubscribeToEvent (string eventToSubscribeTo, ICanRecieveMessages subscriber)
    {
        //if the dictionary doesent have the event it creates one and adds the new listener to it
        if (!eventsAndSubscribers.ContainsKey(eventToSubscribeTo))
        {
            List<ICanRecieveMessages> newList = new List<ICanRecieveMessages>();
            newList.Add(subscriber);
            eventsAndSubscribers.Add(eventToSubscribeTo, newList);
        }
        //otherwise it just adds to the list of subscribers for that event
        else
        {
            eventsAndSubscribers[eventToSubscribeTo].Add(subscriber);
        }
    }

    /// <summary>
    /// Removes a listener from the event
    /// </summary>
    /// <param name="eventToUnsubscribeFrom"></param>
    /// <param name="subscriber"></param>
    public static void UnsubscribeToEvent(string eventToUnsubscribeFrom, ICanRecieveMessages subscriber)
    {
        if (eventsAndSubscribers.ContainsKey(eventToUnsubscribeFrom))
        {
            eventsAndSubscribers[eventToUnsubscribeFrom].Remove(subscriber);

            //if theres nothing left in the list remove it from the dictionary
            if(eventsAndSubscribers[eventToUnsubscribeFrom].Count == 0)
            {
                eventsAndSubscribers.Remove(eventToUnsubscribeFrom);
            }
        }
        else
        {
            Debug.Log("subscriber cannot be removed, event "+ eventToUnsubscribeFrom +" does not exist");
        }
    }

    /// <summary>
    /// send a value to all subscibers of an event
    /// </summary>
    /// <param name="eventToTrigger"></param>
    /// <param name="value"></param>
    public static void TriggerEvent<T>(string eventToTrigger,T value)
    {
        if (eventsAndSubscribers.ContainsKey(eventToTrigger))
        {
            foreach (var subscriber in eventsAndSubscribers[eventToTrigger])
            {
                subscriber.ReceiveMessage(eventToTrigger,value);
            }
        }
        else
        {
            Debug.Log(eventToTrigger + " event dosent exist");
        }
    }

    /// <summary>
    /// sends a value to all subscribers of all events
    /// </summary>
    /// <param name="value"></param>
    public static void BroadcastToAll<T>(string eventToTrigger,T value)
    {
        foreach (var e in eventsAndSubscribers)
        {
            foreach (var sub in e.Value)
            {
                sub.ReceiveMessage(eventToTrigger, value);
            }
        }
    }
}
